Source: dmarcts-report-parser
Section: admin
Priority: optional
Maintainer: Mike Gabriel <sunweaver@debian.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/techsneeze/dmarcts-report-parser/
Vcs-Git: https://salsa.debian.org/debian/dmarcts-report-parser.git
Vcs-Browser: https://salsa.debian.org/debian/dmarcts-report-parser

Package: dmarcts-report-parser
Architecture: all
Depends:
 libclass-dbi-mysql-perl,
 libfile-mimeinfo-perl,
 libio-socket-inet6-perl,
 libio-socket-ip-perl,
 libmail-imapclient-perl,
 libmail-mbox-messageparser-perl,
 libmime-tools-perl,
 libperlio-gzip-perl,
 libxml-simple-perl,
 unzip,
 ${misc:Depends},
Description: Perl based tool to parse DMARC reports
 This DMARC reports parser is based on John Levine's rddmarc, extended by
 the following features:
 .
    * Allows one to read messages from an IMAP server and not only from the
      local filesystem.
    * Store much more XML values into the database (for example the missing SPF
      and DKIM results from the policy_evaluated section) and also the entire
      XML for later reference.
    * Needed database tables and columns are created automatically, user only
      needs to provide a database. The database schema is compatible to the one
      used by rddmarc, but extends it by additional fields. Users can switch
      from rddmarc to dmarcts-report-parser without having to do any changes
      to the database by themselves.
